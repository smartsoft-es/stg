#!/bin/sh

GATEWAY_ID=$(cat /sys/class/net/eth0/address | awk '{print toupper($0)}' | sed 's/://g')
MQTT_SERVER="gzih.gozmart.ch"
MQTT_ID="OpenWRT-Presence-Event"
MQTT_TOPIC="gozmart/gateway/${GATEWAY_ID}000020"

iw event | \
while read LINE; do
    if echo $LINE | grep -q -E "(new|del) station"; then
        EVENT=`echo $LINE | awk '/(new|del) station/ {print $2}'`
        MAC=`echo $LINE | awk '/(new|del) station/ {print $4}'`

        #echo "Mac: $MAC did $EVENT"
        mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/event" -m $EVENT
        mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/mac" -m ${MAC//:/}

    fi
done
