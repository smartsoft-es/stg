#!/bin/sh /etc/rc.common

GATEWAY_ID=$(cat /sys/class/net/eth0/address | awk '{print toupper($0)}' | sed 's/://g')
MQTT_SERVER="gzih.gozmart.ch"
MQTT_ID="OpenWRT-Presence-LastSeen"
MQTT_TOPIC="gozmart/gateway/${GATEWAY_ID}500001"

for interface in `iw dev | grep Interface | cut -f 2 -s -d" "`
do
  # for each interface, get mac addresses of connected stations/clients
  maclist=`iw dev $interface station dump | grep Station | cut -f 2 -s -d" "`

  i=0; 
  for token in $maclist; 
   do 
    i=$((i+1));
   done; 

  # for each mac address in that list...
  for mac in $maclist
  do
    # mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC${mac//:/-}/lastseen/iso8601" -m $(date +%Y-%m-%dT%H:%M:%S%z)
    # mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC${mac//:/-}/lastseen/epoch" -m $(date +%s)
    mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/wifidevices" -m "${mac}"
  done

done
mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/wifitotaldevices" -m "$i"

for j in  `cat "/etc/openwrt_version"`
do 
   echo $j

done

#version of the OpenWRT
mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/version" -m "$j"

#local IP of the gateway
mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/ip" -m $(ifconfig eth0|grep inet|head -1|cut -d':' -f 2|cut -d' ' -f1)

#SSID of the gateway
mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/SSID" -m $(uci get wireless.@wifi-iface[0].ssid)

#mac address of the gateway 
mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/mac" -m $(cat /sys/class/net/eth0/address | awk '{print toupper($0)}')

#number of 6lowpan clients
mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/6lowpantotaldevices" -m $( wget -q -O- http://127.0.0.1/cgi-bin/luci/nodes | cut -f1 -d, | cut -f2 -d:)

#running time of the device
mosquitto_pub -h $MQTT_SERVER -i $MQTT_ID -t "$MQTT_TOPIC/uptime" -m $(cut -f1 -d. /proc/uptime)