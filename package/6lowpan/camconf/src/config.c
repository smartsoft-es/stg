#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#include "config.h"
#include "uci.h"
#include "errno.h"
#include "debug.h"

static struct uci_context *ctx=NULL;
static struct uci_ptr OUI_list={0,};

void config_init()
{
	ctx = uci_alloc_context();
	if (!ctx){
		ERR("Error Allocating config contex\n");
		exit(-1);
	}
}

void config_end()
{
	uci_free_context(ctx);
	ctx=NULL;
}


void config_get_api_url(char *url)
{
	struct uci_ptr ptr={0,};

	char uci_string[64]="camconf.@cameras[0].url";
	if (uci_lookup_ptr(ctx, &ptr, uci_string, true)!=UCI_OK){
		ERR("Uci camconf.@cameras[0].url not found\n");
		exit(-1);
	}
	
	if ( ptr.target!=UCI_TYPE_OPTION || !ptr.o || ptr.o->type!=UCI_TYPE_STRING){
		ERR("Invalid value camconf.@cameras[0].url\n");
		exit(-1);
	}
	
	strcpy(url,ptr.o->v.string);
	INFO("api url -> %s\n", url);
}

void config_load_oui_list()
{	
	struct uci_element *e=NULL;

	char uci_string[64]="camconf.@cameras[0].OUI";
	if (uci_lookup_ptr(ctx, &OUI_list, uci_string, true)!=UCI_OK){
		ERR("Uci camconf.@cameras[0].url not found\n");
		exit(-1);
	}
	
	if ( OUI_list.target!=UCI_TYPE_OPTION || !OUI_list.o || OUI_list.o->type!=UCI_TYPE_LIST){
		ERR("Invalid value camconf.@cameras[0].OUI\n");
		exit(-1);
	}
}

int config_check_oui_valid(uint8_t *hwaddr)
{
	struct uci_element *e=NULL;
	char mac[9] = "";
	sprintf(mac, "%x:%x:%X", hwaddr[0],hwaddr[1],hwaddr[2]);
	uci_foreach_element(&OUI_list.o->v.list, e) {
		if(!strcasecmp(mac, e->name)){
			DBG("Checking OUI %s, Valid\n", e->name);
			return 1;
		}else{
			DBG("Checking OUI %s, Invalid\n", e->name);
		}
	}
	return 0;
}