void config_init();
void config_end();
void config_get_api_url(char *url);
void config_load_oui_list();
int config_check_oui_valid(uint8_t *hwaddr);