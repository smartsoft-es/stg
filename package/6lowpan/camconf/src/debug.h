/*DEBUG*/
#ifndef __DEBUG_H
#define __DEBUG_H 1

#include <syslog.h>

#ifdef DEBUG
	#define DBG(...) fprintf(stderr, __VA_ARGS__)
	#define INFO(...) printf(__VA_ARGS__)
	#define ERR(...) printf(__VA_ARGS__)
	#define BUG(...) printf(__VA_ARGS__)
#else
	#define DBG (void)
	#define INFO(format, ...) {char newformat[256]={0,}; sprintf(newformat,"camconf: %s", format); syslog(LOG_INFO, newformat, ##__VA_ARGS__); }
	#define ERR(format, ...) {char newformat[256]={0,}; sprintf(newformat,"camconf: %s", format); syslog(LOG_ERR, newformat, ##__VA_ARGS__); }
	#define BUG(format, ...) {char newformat[256]={0,}; sprintf(newformat,"camconf: %s", format); syslog(LOG_CRIT, newformat, ##__VA_ARGS__); }
#endif


#endif