
#include <libubox/uclient.h>
#include <libubox/blobmsg.h>
#include "debug.h"
#define USER_AGENT "ccamconf daemon"
static int error_ret=0;
static void request_done(struct uclient *cl){
	DBG("Request done\n");
	uclient_disconnect(cl);
	uclient_free(cl);
	uloop_end();
}
static void header_done_cb(struct uclient *cl)
{
	DBG("Status code %d\n",cl->status_code);
}
static void read_data_cb(struct uclient *cl)
{
	DBG("Read CD\n");
	char buf[256] = {0,};
	int len;

	while ( (len = uclient_read(cl, buf, sizeof(buf))) ) {
		DBG("%s\n",buf);
	}
}
static void eof_cb(struct uclient *cl)
{
	DBG("EOF\n");
	if (!cl->data_eof) {
		error_ret = 4;
	} else {
		DBG("Download completed\n");
	}
	request_done(cl);
}
static void handle_uclient_error(struct uclient *cl, int code)
{
	const char *type = "Unknown error";
	bool ignore = false;

	switch(code) {
	case UCLIENT_ERROR_CONNECT:
		type = "Connection failed";
		error_ret = 4;
		break;
	case UCLIENT_ERROR_TIMEDOUT:
		type = "Connection timed out";
		error_ret = 4;
		break;
	case UCLIENT_ERROR_SSL_INVALID_CERT:
		type = "Invalid SSL certificate";
		error_ret = 5;
		break;
	case UCLIENT_ERROR_SSL_CN_MISMATCH:
		type = "Server hostname does not match SSL certificate";
		error_ret = 5;
		break;
	default:
		error_ret = 1;
		break;
	}


	if (ignore)
		error_ret = 0;
	else
		request_done(cl);
	
	ERR("Http Error %s\n",type);
}

static const struct uclient_cb cb = {
	.header_done = header_done_cb,
	.data_read = read_data_cb,
	.data_eof = eof_cb,
	.error = handle_uclient_error,
};/*TODO*/

static int init_request(struct uclient *cl,const char* post_data)
{
	int rc;
	DBG("Connect\n");
	
	rc = uclient_connect(cl);
	if (rc){
		ERR("CONNECT err %d\n",rc);
		return rc;
	}

	DBG("Post\n");
	rc = uclient_http_set_request_type(cl, "POST");
	if (rc)
		return rc;
	DBG("Headers\n");
	uclient_http_reset_headers(cl);
	uclient_http_set_header(cl, "User-Agent", USER_AGENT);
	uclient_http_set_header(cl, "Content-Type", "application/json");
	DBG("Write\n");
	uclient_write(cl, post_data, strlen(post_data));
	DBG("Doing Request\n");
	rc = uclient_request(cl);
	if (rc)
		return rc;
	DBG("New Request\n");
	return 0;
}
static struct uclient* cl;
int post_to_server(const char * data,const char* server){
	DBG("Posting to the server %s\n",server);
	
	int rc;
	DBG("New client\n");
	uloop_init();
	cl = uclient_new(server, NULL, &cb);
	if (!cl){
		DBG("Error Allocating Context\n");
		return 1;/*Error Allocating*/
	}
	DBG("Addr Family\n");
	//uclient_http_set_address_family(cl, AF_INET6); /*Force IPv6 Usage*/
	DBG("Init REQ\n");
	
	uclient_set_url(cl,server, NULL);
	
	rc = init_request(cl,data);
	if (!rc){
		uloop_run();
		uloop_done();
		return 0;
	}
	return error_ret;
}
