/*
 */
#include <string.h>
#include <stdio.h>

#include <netlink/netlink.h>
#include <netlink/genl/genl.h>

#include <arpa/inet.h>
#include <net/if.h>
#include <linux/if_addr.h>
#include <linux/rtnetlink.h>
#include <json-c/json.h>

#include "debug.h"
#include "http.h"
#include "config.h"
#include "mac.h"

#define OFFSET 0xC000 /*First port for nat*/
#define PORT_MASK	0xFFF /*12 Bits for por nat*/
#define NAT_IFACE	"tun-ipv6" /* Interface to open camera ports for NAT */

int portForMac (uint8_t* mac)
{
	int _port=0;

	_port = mac[4];
	_port = _port << 8;
	_port = _port | mac[5];
	_port = ((_port & PORT_MASK) | OFFSET);

 	return _port;
}

void mac_str_to_array(char *mac, uint8_t *hwaddr)
{	
	//https://stackoverflow.com/a/20553913/2115411
	int values[6];
	if( 6 == sscanf( mac, "%x:%x:%x:%x:%x:%x",
		&values[0], &values[1], &values[2],
		&values[3], &values[4], &values[5] )
	){
		/* convert to uint8_t */
		for(int i = 0; i < 6; ++i ){
			hwaddr[i] = (uint8_t) values[i];
		}
	} else {
		ERR("Invalid MAC %s", mac);
		hwaddr[0] = hwaddr[1] = hwaddr[2] = hwaddr[3] = hwaddr[4] = hwaddr[5] = 0;
	}
}

static json_object * generate_json(char *action, char *mac, char *ip, char *hostname)
{
	uint8_t hwaddr[6];
	mac_str_to_array(mac, hwaddr);
	
	uint64_t id = mac2int(hwaddr);
	char str_mac[64] = {0,};
	sprintf(str_mac,"%llX",id);
	
	json_object *json = json_object_new_object();
	json_object_object_add(json, "id", json_object_new_int64(id));
	json_object_object_add(json, "action", json_object_new_string(action));
	json_object_object_add(json, "mac", json_object_new_string(str_mac));
	json_object_object_add(json, "ip", json_object_new_string(ip));
	json_object_object_add(json, "hostname", json_object_new_string(hostname));
	json_object_object_add(json, "port", json_object_new_int(portForMac(hwaddr)));
	return json;
}

static void add_json_camera(json_object * cameras, uint8_t *hwaddr, char *ip, char *hostname)
{
	uint64_t id = mac2int(hwaddr);
	char str_mac[64] = {0,};
	sprintf(str_mac,"%llX",id);
	
	json_object *json = json_object_new_object();
	json_object_object_add(json, "mac", json_object_new_string(str_mac));
	json_object_object_add(json, "ip", json_object_new_string(ip));
	json_object_object_add(json, "hostname", json_object_new_string(hostname));
	json_object_object_add(json, "port", json_object_new_int(portForMac(hwaddr)));
	json_object_array_add(cameras, json);
}

void get_eth0_mac(char * mac)
{
	FILE *pFile;  
	char value[256]= "";

	pFile = popen("cat /sys/class/net/eth0/address", "r");
	if (pFile == NULL) {
		ERR("Failed to run command\n");
		exit(1);
	}

	if ( fgets(value, sizeof(value)-1, pFile) != NULL ) {
		strcpy(mac, value);
	} else {
		ERR("Failed to get eth0 MAC\n");
		pclose(pFile);
		exit(1);
	}

	pclose(pFile);
}

void send_dhcp_leases(uint64_t id, char * api_url)
{
	json_object *json = json_object_new_object();
	json_object_object_add(json,"id",json_object_new_int64(id));
	json_object *json_cameras = json_object_new_array();
	json_object_object_add(json,"cameras",json_cameras);

	FILE *fp;
	char line[256],expiry[256],mac[256],ip[256],hostname[256],clientid[256];
	uint8_t hwaddr[6];

	fp = fopen("/tmp/dhcp.leases", "r");
	if (fp == NULL) {
		ERR("Failed open /tmp/dhcp.leases\n" );
		exit(1);
	}

	//dhcp.leases format http://lists.thekelleys.org.uk/pipermail/dnsmasq-discuss/2006q2/000733.html
	while (fgets(line, sizeof(line)-1, fp) != NULL) {
		if( 5 == sscanf( line, "%s %s %s %s %s", expiry, mac, ip, hostname, clientid )){
			mac_str_to_array(mac, hwaddr);
			if(config_check_oui_valid(hwaddr)){			
				add_json_camera(json_cameras, hwaddr, ip, hostname);
			}
		}	
	}

	/* close */
	pclose(fp);

	INFO("JSON: %s\n",json_object_to_json_string(json));
	post_to_server(json_object_to_json_string(json),api_url);
}

int nat(const char* iface, const char* dst, int port ,char action){
	char str[256];
	sprintf(str,"/usr/sbin/iptables -t nat -%c PREROUTING -i %s -p tcp --dport %i -j DNAT --to %s:554 ",action,iface,port,dst);
	INFO("%s\n",str);
	return system(str);
}

int main(int argc, char **argv) {
	
	char eth0mac[256]= "";
	uint64_t eth0id = 0;
	char api_url[256] = "";
	char action[256] = "";
	char mac[256] = "";
	char ip[256] = "";
	char hostname[256] = "";

	INFO("camconf starting ...\n");
	
	config_init();
	config_get_api_url(api_url);
	config_load_oui_list();

	get_eth0_mac(eth0mac);
	uint8_t hwaddr[6];
	mac_str_to_array(eth0mac, hwaddr);
	eth0id = mac2int(hwaddr);

	if (argc > 3 && argc < 6) {
		strcpy(action, argv[1]);
		strcpy(mac, argv[2]);
		strcpy(ip, argv[3]);
		if(argc==5){
			strcpy(hostname, argv[4]);
		}
		mac_str_to_array(mac, hwaddr);
		int port = portForMac(hwaddr);
        INFO("Running with parameters action: %s mac: %s ip: %s hostname: %s\n", action, mac, ip, hostname);        
		if(config_check_oui_valid(hwaddr)){
			//json_object *json = generate_json(action, mac, ip, hostname);
			if(!strcmp(action, "add")){
				nat(NAT_IFACE, ip, port,'I');
				INFO("NAT added for ip %s on port %d", ip, port);
				send_dhcp_leases(eth0id, api_url);
			}else if(!strcmp(action, "del")){
				nat(NAT_IFACE, ip, port,'D');
				INFO("NAT deleted for ip %s on port %d", ip, port);
				send_dhcp_leases(eth0id, api_url);
			}
		}
    }
	
	//send_dhcp_leases(eth0id, api_url);
	INFO("camconf finished\n");
	config_end();
	return 0;
}
