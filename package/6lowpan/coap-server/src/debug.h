/*DEBUG*/
#ifndef __DEBUG_H
#define __DEBUG_H 1

#include <stdio.h>
#include <syslog.h>


#ifdef __x86_64
	#define TEST
	#undef DEBUGLOG
#else
	#undef TEST
	#define DEBUGLOG
#endif

#ifdef DEBUG
	#ifdef DEBUGLOG
		#define DBG(...) syslog(LOG_DEBUG, __VA_ARGS__)
	#else
		#define DBG(...) fprintf(stderr, __VA_ARGS__)
	#endif
#else
	#define DBG(...) (void)0
#endif

#define INFO(...) syslog(LOG_INFO, __VA_ARGS__)
#define ERR(...) syslog(LOG_ERR, __VA_ARGS__)
#define BUG(...) syslog(LOG_CRIT, __VA_ARGS__)
#endif