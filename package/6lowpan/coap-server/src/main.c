#include <libcoap/coap.h>
#include <netdb.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>

#include "methods.h"
#include "nat.h"
#include "debug.h"

#define MAXHOST 1024
#define MAXPORT 32

coap_context_t * get_context(const char *node, const char *port) {
	coap_context_t *ctx = NULL;
	int s;
	struct addrinfo hints;
	struct addrinfo *result, *rp;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_DGRAM; /* Coap uses UDP */
	hints.ai_flags = AI_PASSIVE | AI_NUMERICHOST;
	
	s = getaddrinfo(node, port, &hints, &result);
	if ( s != 0 ) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		return NULL;
	} 

	/* iterate through results until success */
	for (rp = result; rp != NULL; rp = rp->ai_next) {
		coap_address_t addr;

		if (rp->ai_addrlen <= sizeof(addr.addr)) {
			coap_address_init(&addr);
			addr.size = rp->ai_addrlen;
			memcpy(&addr.addr, rp->ai_addr, rp->ai_addrlen);

			ctx = coap_new_context(&addr);
			if (ctx) 
				break; /* TODO: output address:port for successful binding */
		}
	}
	if (!ctx)
		fprintf(stderr, "no context available for interface '%s'\n", node);

	freeaddrinfo(result);
	return ctx;
}

void init_resources(coap_context_t *ctx) {
	coap_resource_t *r;
	//r = coap_resource_init((const unsigned char*)"nat", strlen("nat"), 0);
	//r = coap_resource_init(NULL, 0, 0);
	r = coap_resource_init((unsigned char *)"nat", 3, 0);
	coap_register_handler(r, COAP_REQUEST_GET, hnd_get_nat);
	//coap_add_attr(r, (unsigned char *)"nat", 3,NULL, 0, 0);
	coap_add_resource(ctx, r);
}

int main(int argc, char *argv[]){
	char coap_addr[MAXHOST] = "::";/*Bind address*/
	char coap_port_str[MAXPORT] = "5683";/*Coap default port (udp)*/
	coap_log_t log_level = LOG_DEBUG;
	
	coap_context_t  *ctx;
	fd_set readfds;
	struct timeval tv, *timeout;
	int result;
	coap_tick_t now;
	coap_queue_t *nextpdu;
	
	/*Init Coap*/
	coap_set_log_level(log_level);
	ctx = get_context(coap_addr, coap_port_str);
	if (!ctx)  return -EPERM;
	
	restore_nat();
	
	init_resources(ctx);
	
	while ( 1 ) {
		FD_ZERO(&readfds);
		FD_SET( ctx->sockfd, &readfds );

		nextpdu = coap_peek_next( ctx );

		coap_ticks(&now);
		while ( nextpdu && nextpdu->t <= now ) {
			coap_retransmit( ctx, coap_pop_next( ctx ) );
			nextpdu = coap_peek_next( ctx );
		}

		if ( nextpdu && nextpdu->t <= now + COAP_RESOURCE_CHECK_TIME ) {
			/* set timeout if there is a pdu to send before our automatic timeout occurs */
			tv.tv_usec = ((nextpdu->t - now) % COAP_TICKS_PER_SECOND) * 1000000 / COAP_TICKS_PER_SECOND;
			tv.tv_sec = (nextpdu->t - now) / COAP_TICKS_PER_SECOND;
			timeout = &tv;
		} else {
			tv.tv_usec = 0;
			tv.tv_sec = COAP_RESOURCE_CHECK_TIME;
			timeout = &tv;
		}

		result = select( FD_SETSIZE, &readfds, 0, 0, timeout );

		if ( result < 0 ) {	/* error */
			if (errno != EINTR){
				perror("select");
			}
		} else if ( result > 0 ) {	/* read from socket */
				if ( FD_ISSET( ctx->sockfd, &readfds ) ) {
					coap_read( ctx );	/* read received data */
				}
		}
	}
	
}