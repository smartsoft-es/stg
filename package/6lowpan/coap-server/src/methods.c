#include <stdio.h>
#include <libcoap/net.h>

#include "methods.h"
#include "debug.h"

#include "nat.h"

/*Get http url from coap pdu*/

void 
hnd_get_nat(coap_context_t  *ctx, struct coap_resource_t *resource,
	     const coap_endpoint_t *local_interface,
	     coap_address_t *peer, coap_pdu_t *request, str *token,
	     coap_pdu_t *response){
	
	DBG("Packet recived Funcion %s\n",__FUNCTION__);

	if (add_nat(peer->addr.sin6))
		response->hdr->code = COAP_RESPONSE_CODE(500);
	else
		response->hdr->code = COAP_RESPONSE_CODE(205);
	
}