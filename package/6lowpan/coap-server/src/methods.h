#ifndef _METHODS_H_
#define _METHODS_H_
#include <libcoap/net.h>

void 
hnd_get_nat(coap_context_t  *ctx, struct coap_resource_t *resource,
	     const coap_endpoint_t *local_interface,
	     coap_address_t *peer, coap_pdu_t *request, str *token,
	     coap_pdu_t *response);

#endif