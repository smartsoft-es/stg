#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <uci.h>
#include "debug.h"
#include "nat.h"
/*TODO calc dst port*/


#define OFFSET 0xC000 /*First port for nat*/
#define PORT_MASK	0xFFF /*12 Bits for por nat*/

static struct uci_context *ctx=NULL; /*to save_data*/
static struct uci_package* pkg=NULL;
int remove_nat(char* value);
/*
	struct in6_addr {
		u_int8_t  s6_addr[16];
	}
*/
int port (struct sockaddr_in6 ip6){
	struct in6_addr ip6_addr=ip6.sin6_addr;
	int _port=0;

	_port=ip6_addr.s6_addr[14];
	_port=_port<<8;
	_port=_port|ip6_addr.s6_addr[15];
	_port=((_port&PORT_MASK)|OFFSET);

 	return _port;
}

static void ipv6_addr(struct sockaddr_in6 ip6,char* buff,int buff_size) {
	inet_ntop(AF_INET6, &(ip6.sin6_addr), buff, buff_size);
	DBG("%s %s\n",__FUNCTION__,buff);
}

int look_for_value (const char* value){
		struct uci_element *s,*e,*o;
		uci_foreach_element(&pkg->sections, s){
		uci_foreach_element(&uci_to_section(s)->options,o){
			uci_foreach_element(&uci_to_option(o)->v.list, e){
				if (!strcmp(uci_to_option(e)->e.name,value)){
					DBG("FIND VALUE [%s]\n",uci_to_option(e)->e.name);
					return 1;
				}
 			}
		}
	}
	return 0;
}

int nat (const char* iface, const char* dst, int port ,char action){
	char str[256];
	//ip6tables -t nat -I PREROUTING -i tun-ipv6 -p udp --dport 52974 -j DNAT --to [aaaa::212:4b00:40e:eeee]:5683
	sprintf(str,IPTABLES" -t nat -%c PREROUTING -i %s -p udp --dport %i -j DNAT --to [%s]:5683 ",action,iface,port,dst);
	DBG("SYSTEM [%s]\n",str);
	return system(str);
}

int del_nat(struct sockaddr_in6 ip6){
	char remote_addr [INET6_ADDRSTRLEN];
	ipv6_addr(ip6,remote_addr,INET6_ADDRSTRLEN);
	remove_nat(remote_addr);
	return nat(DEF_IFACE,remote_addr,port(ip6),'D');
}

int add_nat(struct sockaddr_in6 ip6){
	char remote_addr [INET6_ADDRSTRLEN];
	ipv6_addr(ip6,remote_addr,INET6_ADDRSTRLEN);

	if (look_for_value(remote_addr))
		return 2;/*Nat is done*/

	nat(DEF_IFACE,remote_addr,port(ip6),'D');
	save_nat(remote_addr);
 	return nat(DEF_IFACE,remote_addr,port(ip6),'I');
}

int flush_nat(void){
	return system(IPTABLES" -t nat -F PREROUTING");
}
/*Load And save config retlated functions*/
int load_pkg(void){
	ctx=uci_alloc_context();
	if (!ctx)
		return -1;
	return uci_load(ctx,UCI_PKG,&pkg);
}
int free_config(void){
	uci_unload(ctx,pkg);
	uci_free_context(ctx);
	ctx=NULL;
	return 0;
}

struct uci_option* get_option(void){
	struct uci_element *s,*e,*o;

	uci_foreach_element(&pkg->sections, s){
		DBG("secction %s\n",uci_to_section(s)->type);
		uci_foreach_element(&uci_to_section(s)->options,o){
		DBG("Option [%s]\n",o->name);/*YES = NAT*/
			uci_foreach_element(&uci_to_option(o)->v.list, e){ /*Type list*/
				DBG("Type [%i]\n",e->type);/*Type option*/
				DBG("Type [%i]\n",uci_to_option(e)->type);/*Type 1=STRING*/
				DBG("STR [%s]\n",uci_to_option(e)->v.string);/*Type 1=STRING*/
				DBG("STRNAME [%s]\n",uci_to_option(e)->e.name);/*OK Valor*/
				return uci_to_option(e);
 			}
		}
	}
	return NULL;
}

struct uci_section* get_section(void){
	struct uci_element *s,*o;

	uci_foreach_element(&pkg->sections, s){
		DBG("secction %s\n",uci_to_section(s)->type);
		uci_foreach_element(&uci_to_section(s)->options,o){
		DBG("Option [%s]\n",o->name);/*YES = NAT*/
			return (uci_to_section(s));
		}
	}
	return NULL;
}

int save_nat(char* value){
	struct uci_ptr ptr={0,};
	struct uci_section* section=NULL;
	char opt_name[64]=UCI_OPT;

	ptr.p=pkg;
	ptr.value=value;
	ptr.option=opt_name;

	section = get_section();

	if (!section){ /*if option do not exits, create it*/
		DBG("NO option SAVE NAT\n");
		uci_add_section(ctx, pkg, UCI_SECTION, &section);
		ptr.s=section;/*section name debe de estar a NULL para añadirlo*/
	}else{
		ptr.s=section;
	}
	uci_add_list(ctx,&ptr);
	DBG("Saving value UCI %s\n",value);
	uci_commit(ctx,&pkg,0);
	uci_save(ctx,pkg);
	return (1);
}
int remove_nat(char* value){
	struct uci_ptr ptr={0,};
	struct uci_section* section=NULL;
	char opt_name[64]=UCI_OPT;
	section = uci_lookup_section(ctx,pkg, UCI_SECTION);
	if (!section){
		return -1;
	}
	ptr.p=pkg;
	ptr.s=section;
	ptr.value=value;
	ptr.option=opt_name;
	return (uci_del_list(ctx,&ptr));
}
int restore_nat(void){
	 	load_pkg();
		struct uci_element *s,*e,*o;
		struct sockaddr_in6 ip_addr;
		uci_foreach_element(&pkg->sections, s){
		uci_foreach_element(&uci_to_section(s)->options,o){
			uci_foreach_element(&uci_to_option(o)->v.list, e){
				DBG("RESTORE NAT IP [%s]\n",uci_to_option(e)->e.name);
				inet_pton(AF_INET6,uci_to_option(e)->e.name,&(ip_addr.sin6_addr));
				nat(DEF_IFACE,uci_to_option(e)->e.name,port(ip_addr),'I');/*From str to ip6addr*/
 			}
		}
	}
	return 0;
}
