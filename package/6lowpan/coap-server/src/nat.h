#include <netinet/in.h>
#define IPTABLES "/usr/sbin/ip6tables"
//#define IPTABLES "/bin/echo"
#define DEF_IFACE	"tun-ipv6"

#define UCI_PKG "6lowpannat"
#define UCI_SECTION "clients"
#define UCI_SECTION_NAME "tun-ipv6"
#define UCI_OPT	"nat"
#define UCI_STR "6lowpannat.clients.nat"

int del_nat(struct sockaddr_in6 ip6);
int add_nat(struct sockaddr_in6 ip6);
int flush_nat(void);
int restore_nat(void);
int save_nat(char* ip6_addr);
