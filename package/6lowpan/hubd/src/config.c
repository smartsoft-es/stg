#include <string.h>
#include <stdlib.h>

#include "config.h"
#include "uci.h"
#include "errno.h"
#include "debug.h"

typedef struct {
	char name[16]; /*uci name of the package*/
	struct uci_package* pkg;
} uci_packages;

static struct uci_context *ctx=NULL;

static uci_packages packages[] = {
	{
		.name = UCI_PKG_SERVER,
		.pkg = NULL,
	},
	{
		.name = {0,},
		.pkg = NULL,
	}
};

int init_config(){
	int i=0;
	ctx=uci_alloc_context();

	if (!ctx){
		ERR("Error Allocating config contex\n");
		return -ENOMEM;
	}
	
	while (packages[i].name[0]){
		DBG("Loading %s\n",packages[i].name);
		uci_load(ctx,packages[i].name,&packages[i].pkg);
		i++;	
	}
	return i;
}

void free_config(){
	int i=0;
	while (packages[i].name[0]){
		DBG("Freeing Package %s \n",packages[i].name);
		uci_unload(ctx,packages[i].pkg);
		i++;	
	}
	uci_free_context(ctx);
	ctx=NULL;
}

int get_config_value(char *uci_value, char* value){
	struct uci_ptr ptr={0,};

	if (uci_lookup_ptr(ctx,&ptr,uci_value,true)!=UCI_OK)
		return -ENOENT; /*The elemet NOT exits*/
	
	if ( ptr.target!=UCI_TYPE_OPTION || !ptr.o || ptr.o->type!=UCI_TYPE_STRING)
		return -ENOENT;
	
	strcpy(value,ptr.o->v.string);
	return 0;
}

static char url [128];
char* get_server(void){
	char uci_value[64]="server.@server[0].url";
	get_config_value(uci_value, url);
	return url;
}
