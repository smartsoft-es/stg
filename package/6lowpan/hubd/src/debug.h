/*DEBUG*/
#ifndef __DEBUG_H
#define __DEBUG_H 1

#include <syslog.h>

#ifdef DEBUG
	#ifdef DEBUGLOG
		#define DBG(...) syslog(LOG_DEBUG, __VA_ARGS__)
	#else
		#define DBG(...) fprintf(stderr, __VA_ARGS__)
	#endif
	#define INFO(...) printf(__VA_ARGS__)
	#define ERR(...) printf(__VA_ARGS__)
	#define BUG(...) printf(__VA_ARGS__)
#else
	#define DBG (void)
	#define INFO(...) syslog(LOG_INFO, __VA_ARGS__)
	#define ERR(...) syslog(LOG_ERR, __VA_ARGS__)
	#define BUG(...) syslog(LOG_CRIT, __VA_ARGS__)
#endif


#endif