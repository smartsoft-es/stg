/*
 */
#include <string.h>
#include <stdio.h>

#include <netlink/netlink.h>
#include <netlink/genl/genl.h>

#include <arpa/inet.h>
#include <net/if.h>
#include <linux/if_addr.h>
#include <linux/rtnetlink.h>
#include <json-c/json.h>

#include "debug.h"
#include "http.h"
#include "config.h"
#include "mac.h"

#define ID_IFACE "eth0"
static uint64_t id=0;
static int update;/*first update*/
/*remove value from array, returns a new array without the element*/
json_object * json_object_array_del(json_object * array, char * value){
	int i=0;
	json_object * arr_val;
	json_object * new_arr=json_object_new_array();
	for (i=0; i < json_object_array_length(array); i++){
		arr_val=json_object_array_get_idx(array,i);
		if (strcmp(value,json_object_get_string(arr_val))) {
			json_object_array_add(new_arr,	
						json_object_new_string(
							json_object_get_string(arr_val)
						)
					);
		}
	}
	return new_arr;
}
json_object * json_object_array_find(json_object * array, char * value){
	int i=0;
	json_object * arr_val;
	for (i=0; i < json_object_array_length(array); i++){
		arr_val=json_object_array_get_idx(array,i);
		if (!strcmp(value,json_object_get_string(arr_val))) {
			return arr_val;
		}
	}
	return NULL;
}

/*return interface object or create it*/
json_object * get_iface (char* ifname,json_object *json_ifaces){
	json_object *json_if;
	if (!json_object_object_get_ex(json_ifaces,ifname,&json_if)){
		json_if=json_object_new_object();
		json_object_object_add(json_ifaces,ifname,json_if);
		DBG("New Object\n");
	}
	return json_if;
}

static int addr_cb(struct nlmsghdr *nlh, json_object *json_ifaces) {
	
	struct ifaddrmsg *iface = NLMSG_DATA(nlh);

	char ifname[IF_NAMESIZE]={0,};
	int len = NLMSG_LENGTH(nlh->nlmsg_len);
	int rtlen=0;
	struct rtattr *retrta;
	char pradd[128];
	json_object *json_if;
	json_object *json_if_addr_arr;
	json_object *_json_if_addr_arr;
	
	DBG("Net Link Callback\n");
	
	if (!if_indextoname(iface->ifa_index,ifname))
		return 0;
	
	DBG("Interface %s\n",ifname);
	json_if=get_iface(ifname,json_ifaces);
	DBG("Adding Ipaddres to %s %s\n",ifname,json_object_to_json_string(json_if));
	
	
	if (!json_object_object_get_ex(json_if,"addrs",&json_if_addr_arr)){
		json_if_addr_arr=json_object_new_array();
		json_object_object_add(json_if,"addrs",json_if_addr_arr);
	}
	
	while NLMSG_OK(nlh, len) {
		retrta = (struct rtattr *)IFA_RTA(iface);
		
		rtlen = IFA_PAYLOAD(nlh);
		for (; RTA_OK(retrta, rtlen); retrta = RTA_NEXT(retrta, rtlen)) {
			if(retrta->rta_type == IFA_ADDRESS){
				INFO("Change on interfaces\n");
				inet_ntop(iface->ifa_family, RTA_DATA(retrta), pradd, sizeof(pradd));
				sprintf(pradd,"%s/%u", pradd, iface->ifa_prefixlen);
				if (nlh->nlmsg_type==RTM_NEWADDR){
					
					if (json_object_array_find(json_if_addr_arr,pradd))
						continue;/*interface's addr is in*/
					
					json_object_array_add(json_if_addr_arr,json_object_new_string(pradd));
					
					if(strncmp(pradd,"fe80",4)){
						INFO("Force Update to the server\n");
						update=1;/*if the new ipaddr is a glopab ipv6 address update!*/
					}
				}else if (nlh->nlmsg_type==RTM_DELADDR){
					DBG("Del Addr [%s]\n",pradd);
					/*replace the Array*/
					DBG("Length is %i!!\n",json_object_array_length(json_if_addr_arr));
					if (json_object_array_length(json_if_addr_arr)==1){ /*Last element of the interface, remove the entire iface*/
						json_object_object_del(json_if,"addrs");
					}else{
						_json_if_addr_arr=json_object_array_del(json_if_addr_arr,pradd);
						json_object_object_del(json_if,"addrs");
						json_object_object_add(json_if,"addrs",_json_if_addr_arr);
					}
				}
				DBG("%s\n",json_object_to_json_string(json_if));
			}
		}
		nlh = NLMSG_NEXT(nlh, len);
	}

	return 0;
}

void link_cb(struct nlmsghdr *h,json_object *json_ifaces){
	struct ifinfomsg *iface;
	struct rtattr *attribute;
	int len;
	int id_face=0;
	char str_mac[64];
	uint8_t * mac;
	json_object *json_if;
	json_object *json_id;
	char* ifname;
	iface = NLMSG_DATA(h);
	len = h->nlmsg_len - NLMSG_LENGTH(sizeof(*iface));

	for (attribute = IFLA_RTA(iface); RTA_OK(attribute, len); attribute = RTA_NEXT(attribute, len))
	{
		switch(attribute->rta_type)
		{
			case IFLA_IFNAME: 
				ifname=(char *) RTA_DATA(attribute);
				DBG("Interface %d : %s\n", iface->ifi_index, ifname);
				if (h->nlmsg_type==RTM_NEWLINK){
					json_if=get_iface(ifname,json_ifaces);
					if (!strcmp(ifname,ID_IFACE)){
						id_face=1;
					}
				}else{
					if (json_object_object_get_ex(json_ifaces,ifname,&json_if)){
						json_object_object_del(json_ifaces,ifname);
					}
				}
			break;
			case IFLA_ADDRESS:
				if (h->nlmsg_type==RTM_DELLINK)
					continue;
				mac = (uint8_t *)RTA_DATA(attribute);
				DBG("Source MAC address:%02x:%02x:%02x:%02x:%02x:%02x\n",
					mac[0]& 0xff,mac[1]& 0xff,mac[2]& 0xff,
					mac[3]& 0xff,mac[4]& 0xff,mac[5]& 0xff);
				if (mac[0]){
					if (id_face){
						id=mac2int(mac);
						DBG("%llX\n",id);
						id_face=0;
					}
					sprintf(str_mac,"%llX",mac2int(mac));
					json_if=get_iface(ifname,json_ifaces);
					json_id=json_object_new_string(str_mac);
					json_object_object_add(json_if,"mac",json_id);
				}
			break;
			default:
			break;
		}
	}
	DBG("%s\n",json_object_to_json_string(json_ifaces));
}

static int callback(struct nl_msg *msg, void *arg) {
	struct nlmsghdr *nlh = nlmsg_hdr(msg);
	if (nlh->nlmsg_type == RTM_NEWLINK || nlh->nlmsg_type == RTM_DELLINK)
		link_cb(nlh,(json_object *)arg);
	else if (nlh->nlmsg_type == RTM_NEWADDR || nlh->nlmsg_type == RTM_DELADDR)
		addr_cb(nlh,(json_object *)arg);
	return 0;
}


int main() {
	init_config();
	// Init Json Objet.
	json_object *json = json_object_new_object();
	json_object * json_ifaces = json_object_new_object();
	json_object_object_add(json,"ifaces",json_ifaces);
	struct rtgenmsg addr_hdr = { .rtgen_family = AF_PACKET, };
	struct rtgenmsg link_hdr = { .rtgen_family = AF_UNSPEC, };
	//Init Sockect
	struct nl_sock *socket = nl_socket_alloc();  // Allocate new netlink socket in memory.
	nl_socket_disable_seq_check(socket);
	nl_connect(socket, NETLINK_ROUTE);  // Create file descriptor and bind socket.
	
	/*Get MAC address*/
	nl_socket_modify_cb(socket, NL_CB_VALID, NL_CB_CUSTOM, callback, json_ifaces);
	nl_send_simple(socket, RTM_GETLINK, NLM_F_REQUEST | NLM_F_ROOT, &link_hdr, sizeof(link_hdr));
	nl_recvmsgs_default(socket);
	
	/*Add id*/
	json_object_object_add(json,"id",json_object_new_int64(id));
	/*Regiter to event*/
	nl_socket_add_membership(socket, RTNLGRP_IPV6_IFADDR);
	nl_socket_add_membership(socket, RTNLGRP_IPV4_IFADDR);
	nl_socket_add_membership(socket, RTNLGRP_LINK);
	// Send request for all network interfaces.
	nl_send_simple(socket, RTM_GETADDR, NLM_F_REQUEST | NLM_F_DUMP, &addr_hdr, sizeof(addr_hdr));
	
	do{	
		nl_recvmsgs_default(socket);
		if (update){
			INFO("Updating %s\n",json_object_to_json_string(json));
			post_to_server(json_object_to_json_string(json),get_server());
			update=0;
		}

	}while(1);
	free_config();
	nl_socket_free(socket);
	json_object_put(json);
	return 0;
}
